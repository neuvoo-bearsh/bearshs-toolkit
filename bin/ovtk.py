# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/mg/.spyder/.temp.py

[Description]
short:
long:

[GentooBug]
bug:
url:
titel:
status:
resolution:
reporter:

[UpstreamBug]
bugzilla:
bug:
url:
titel:
status:
resolution:
reporter:
"""


from __future__ import print_function
from optparse import OptionParser
import ConfigParser
import sys, os

FNAME = "NeuvooLog"

config = ConfigParser.ConfigParser()

def readInput(text, singleline=None):
    user_input = []
    try:
        entry = raw_input(text) #"Enter short description:\n")
    except EOFError:
        return None
    if singleline:
        return entry
    else:
        while True:
            user_input.append(entry)
            try:
                entry = raw_input("")
            except EOFError:
                break
        return '\n'.join(user_input)


def commandAdd(options, args):
    if options.filename:
        filename = options.filename
    else:
        filename = FNAME
        
    config.read(filename)
    
    while True:
        short_des = readInput("Enter short description:\n", True)
        if config.has_option("Description", "short") == True:
            if config.get("Description", "short") == '' and (short_des == None or short_des == ''):
                print("you need to specify a short description")
            else:
                break
        elif short_des == None:
            print("you need to specify a short description")
        else:
            break
    
    long_des = readInput("Enter long description:\n")
    
    if config.has_section("Description") == False:
        config.add_section("Description")
    if short_des != '':
        config.set("Description", "short", short_des)
    if long_des != '':
        config.set("Description", "long", long_des)
    
    gbn = readInput("Enter Gentoo Bug #:\n", True)
    if gbn != '':
        while True:
            try:
                int(gbn)
                if config.has_section("GentooBug") == False:
                    config.add_section("GentooBug")
                config.set("GentooBug", "bug", gbn)
                break
            except ValueError:
                print("no valid bug id")
            except TypeError:
                break
            gbn = readInput("Enter Gentoo Bug #:\n", True)
        
    ugn = readInput("Enter Upstream Bug #:\n", True)
    if gbn != '':
        while True:
            try:
                int(gbn)
                if config.has_section("UpstreamBug") == False:
                    config.add_section("UpstreamBug")
                config.set("UpstreamBug", "bug", gbn)
                break
            except ValueError:
                print("no valid bug id")
            except TypeError:
                break
            gbn = readInput("Enter Upstream Bug #:\n", True)
    
    
    with open(filename, 'wb') as configfile:
        config.write(configfile)
        
    print('\n##########')
    os.system("cat NeuvooLog")
    

def commandDiff(options, args):
    for arg in args:
        if os.path.isfile(arg):
            filepath = os.path.abspath(arg)
            lcat = []
            head = filepath
            while True:
                (head, tail) = os.path.split(head)
                if head == '' or head == '/':
                    print("could not detect a portdir/overlay, make sure a 'profile' folder exists")
                    sys.exit()
                lcat.append(tail)
                if os.path.isdir(os.path.join(head, "profiles")):
                    origfile = "/usr/portage"
                    for i in range(len(lcat)):
                        origfile = os.path.join(origfile, lcat.pop())
                    if os.path.isfile(origfile):
                        os.system("diff -u "+origfile+' '+filepath)
                    else:
                        os.system("diff -u /dev/null"+' '+filepath)
                    break
        elif os.path.isdir(arg):
            for item in os.listdir(arg):
                commandDiff(None, [os.path.join(arg,item)])
        else:
            print("file: ", arg, " does not exist")
            sys.exit()
    if options != None:
        sys.exit()
    

help = """usage: NVNV [command] [options] [args]

Commands:
  add           add a describtion
  diff          print diff between a file or directory and equivalent
                in /usr/portage/
  up            update information from bugs.gentoo.org
                and upstream bugzilla if supported
  goto -g|-u    goto bugtracker
  show          show summary
  help          show this text

type NVNV <command> --help"""

def printHelp():
    print(help)

parserAdd = OptionParser(usage="usage: NVNV add [options]")
parserAdd.add_option("-f", "--file", dest="filename",
                  help="write report to FILE", metavar="FILE")
parserAdd.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

parserDiff = OptionParser(usage="usage: NVNV diff <file|dir[, ...]>")
parserDiff.add_option("-r", action="store_true", dest="reverse",
                  help="do the diff in reverse order (not implemented yet)")
parserDiff.add_option("-o", action="append", dest="diffopt", metavar="OPTION",
                  help="add extra options to diff (not implemented yet)")

if len(sys.argv) >= 2:
    if sys.argv[1] == "add":
        (options, args) = parserAdd.parse_args(sys.argv[2:])
        commandAdd(options, args)
    elif sys.argv[1] == "show":
        pass
    elif sys.argv[1] == "diff":
        (options, args) = parserDiff.parse_args(sys.argv[2:])
        commandDiff(options, args)
    else:
        printHelp()
        sys.exit()
else:
    printHelp()
    sys.exit()


print(options)
print(args)



